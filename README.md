# tree(s) sort(ed)

Poster generator and web interface for a collection of images based on the tree sort algorithm.

In the folder `posters` a script to generate a collection of posters using graphviz. Install the requirements and run `posters/posters.py` to generate a collection of svg-files.

In the folder `webinterface` a flask app generating an interface as accesible here: https://frart.algoliterarypublishing.net/treesort/