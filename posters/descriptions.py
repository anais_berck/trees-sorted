descriptions = [
  (
    "images/20211019_151924.jpg",
    "Two white people are sitting at a table typing on their computer keyboards under a roof window. On the table are cups, mandarines and water bottles; on the wall, black and white pictures of statues and people. The mood is studious.",
    (147, 169, 183)
  ),
  (
    "images/20211019_162946.jpg",
    "A tree is standing next to a lake, as if about to jump in it. On the ground, a mix of fallen leaves and grass. The weather is sunny and the ambiance is peaceful.",
    (119, 115, 84)
  ),
  (
    "images/20211022_101225.jpg",
    "Several tree branches form a canopy through which one can see the sky. The leaves go from translucent to opaque.",
    (96, 102, 75)
  ),
  (
    "images/20211025_113835.jpg",
    "A white person is standing between a desk and a blackboard, holding a mic while typing on a keyboard and is giving a presentation to an audience of white masked people.",
    (62, 81, 88)
  ),
  (
    "images/20211025_113843.jpg",
    "Masked students are sitting in an auditorium, they watch and listen. A white person is sitting in front. Large windows appear in the back of the students.",
    (131, 114, 109)
  ),
  (
    "images/20211025_151107.jpg",
    "An indigenous person talks in a microphone while a slide is shown in the back. The slide shows four indigenous people of different ages at the left, it mentions the word family tree at the left.",
    (151, 150, 148)
  ),
  (
    "images/20211025_152322.jpg",
    "A white person speaks in the microphone to two people sitting down at a desk in front of a white screen. One of them is from indigenous background, the other from hispanic descent. On the desk are two laptops. One is showing text. A white person with glasses looks at the scene.",
    (50, 43, 37)
  ),
  (
    "images/20211025_172905.jpg",
    "A white person with long grey hair speaks in the microphone. In the back is a projection screen showing a slide with text. The title of the slide is Nature-Culture.",
    (252, 254, 253)
  ),
  (
    "images/20211026_145331.jpg",
    "Three young people are touching trees in a forest. The tree in the middle is very large and is being hugged by one of the three people.",
    (115, 125, 112)
  ),
  (
    "images/20211026_145339.jpg",
    "Three young people are touching young beech trees in the forest. One person is bending down to a small sapling.",
    (116, 123, 113)
  ),
  (
    "images/20211027_111356.jpg",
    "Nine young people of mixed origin are standing in a room with naked walls. They take on specific positions in the room, forming a tree. In front stands a taller masked person with the back to the camera.",
    (255, 255, 255)
  ),
  (
    "images/20211027_125357.jpg",
    "A sunny image showing a clean swimming pool with black squares at the bottom of the water. People are sitting in small groups next to the swimmingpool, concentrated on something that lies between them.",
    (88, 110, 112)
  ),
  (
    "images/20211027_125504.jpg",
    "Nine people stand next to a swimmingpool. Two people are sitting. One person bends over next to them. It is a sunny day. They discuss a presentation on two white sheets that are lying on the tiles.",
    (89, 87, 76)
  ),
  (
    "images/20211027_125747.jpg",
    "On a sheet of paper about a dozen flowers without stems, varying in color. They cast a sharp, long shadow; the sun is bright and low. All but one flower is put in the top-half of the sheet.",
    (182, 182, 184)
  ),
  (
    "images/20211027_130052.jpg",
    "About a dozen thumbnail-sized rocks are laid in a straight line on the pavement. The light is bright and the rocks cast a sharp shadow. The rocks are similar in color but vary in shape. While the arrangement is deliberate the system is not clear.",
    (255, 255, 255)
  ),
  (
    "images/20211027_130250.jpg",
    "Thumbnail-sized rocks, mushrooms, a chestnut, leaves and flowers are laid out on a surface. The arrangement is careful and deliberate. It seems as they have been arranged in two arms or strings, the string on the left consists of brown, dried leaves and the one on the right contains the flowers. The stones and chestnut are put in the middle the two strings meet.",
    (143, 132, 130)
  ),
  (
    "images/20211027_140938.jpg",
    "A group of people is seen from the back, it seems they are entering a park. The weather is nice. Two trees in the foreground frame the picture and give the suggestion of an entrance.",
    (253, 255, 254)
  ),
  (
    "images/20211027_152654.jpg",
    "Six people stand in a clearing in a forest. They are seen on the back. The people look in the same direction, away from the camera, and do not seem to interact with each other.",
    (126, 126, 124)
  ),
  (
    "images/Anais_berck-6.jpg",
    "A small houseplant in a rustic pot. The plant has wires attached to it which lead to a chipboard which in turn is connected to a powerbank. In the foreground two laptops complete the contrast between the rigid shapes of the technology and the leaves of the plant.",
    (225, 212, 196)
  ),
  (
    "images/forest_walk-11.jpg",
    "A tree seen from the beneath along its trunk. In the bright light, the branches of the tree are reduced to dark shapes. The shape and dark color of the branches contrasts with the translucency and refined shapes of the leaves.",
    (80, 93, 35)
  ),
  (
    "images/forest_walk-6.jpg",
    "A forest, two people, seen from the back walk in it. The light is bright but the people wear coats. The photo is taken from afar and the people look small next to the trees.",
    (71, 103, 66)
  ),
  (
    "images/IMG_9369.JPG",
    "On a tree stump a group of mushrooms bloom up from the center of the stump. The mushrooms are mid-sized.",
    (115, 115, 106)
  ),
  (
    "images/IMG_9372.JPG",
    "A tree with three main trunks sits in the middle of the image, beyond the trunks are the woodlands of the forest.",
    (110, 112, 102)
  ),
  (
    "images/IMG_9379.JPG",
    "Two fingers of white people hold down a sheet of paper. One of the fingernails is painted with black nailpolish. The paper holds a map of interwoven concepts, organised for a talk.",
    (123, 154, 244)
  ),
  (
    "images/IMG_9380.JPG",
    "A white person stands holding a microphone. Projected onto a screen behind them is the text '\"A tree is never felled for the reason it has been planted.\" ............1800.............2000.............2200....'",
    (207, 223, 238)
  ),
  (
    "images/IMG_9397.JPG",
    "Four white people are in a room, one stands and holds a microphone and is speaking, the other three are wearing face masks. Everyone is looking at a projected screen with a pad on it. The first line reads \"14:10-14:30, ORACLE, 1. Octavia Butler: lier ce qu'on fait artistiquement a la politique, 4 questions.\"",
    (76, 87, 70)
  ),
  (
    "images/IMG_9405.JPG",
    "Many people are in a room, four are infront of a chalkboard and projector. Some are gazing up at the projected screen, others are gazing downwards. On the projected screen are marker, pencil and crayon drawings on sheets of paper of plant life.",
    (78, 94, 84)
  ),
  (
    "images/IMG_9412.JPG",
    "Six people stand infront of a chalkboard, presumibly giving a presentation. They are of similar height but of various cultural backgrounds. Some are wearing face masks others aren't, behind them is a projection that is not so clear.",
    (189, 170, 138)
  ),
  (
    "images/IMG_9415.JPG",
    "Three people stand and sit infront of a chalkboard with a projection above them. Two white people are holding a drawing taught, and one person of color is speaking into a microphone. On the projection above their heads is a hand with red paint on the fingertips.",
    (175, 159, 143)
  ),
  (
    "images/IMG_9417.JPG",
    "Three people stand and sit infront of a chalkboard with a projection above them. Two white people are holding a drawing loosely and one person of color is speaking into a microphone. On the projection above their heads is an image of a path through a forest.",
    (86, 96, 85)
  ),
  (
    "images/IMG_9422.JPG",
    "Five white people are standing before an audience and in front of a projected image. One of these people is giving a presentation while the four others are masked. The person on the far right has their mask falling under their nose which is discovering their smile. On the projected image is an electronic device connected to a plant. The ambiance is joyful.",
    (134, 126, 117)
  ),
  (
    "images/IMG_9424.JPG",
    "One white person is standing before an audience and in front of a projected image and is talking into a small microphone. Another person on the far left is looking at the projected image, on it, there is a graphic diagram of a system including a computer, a coffee machine and a plant with the words \"voice recognition\" and \"kindness list\".",
    (65, 66, 58)
  ),
  (
    "images/IMG_9432.JPG",
    "Four people are standing before an audience and in front of a projected image. All are masked except for one who bends to reach a keyboard. The person on the far left is looking at the projected image, on it, there is the glitched representation of a bag of crips.",
    (219, 217, 218)
  ),
  (
    "images/IMG_9435.JPG",
    "Four people are standing before an audience and in front of a projected image on which is presented a new alphabet. The person in the middle is talking in a microphone while pointing at a device that the person on the far right is holding. A clock on the wall indicates 4:07.",
    (116, 104, 90)
  ),
  (
    "images/IMG_9436.JPG",
    "One white person is standing before an audience, in front of a projected image and is talking into a small microphone while looking at a computer. The projected image is an otherwordly drawing of the forest.",
    (67, 76, 71)
  ),
  (
    "images/Saint_Luc.jpg",
    "Three people hold up two A3-sized sheets, they are giving a presentation. The picture is taken from the audience. The sheets contain small illustrations of leaves and animals, the illustrations were cut, and recomposed to fill the sheet like a pattern.",
    (94, 100, 96)
  )
]