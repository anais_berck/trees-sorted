import textwrap
from string import ascii_uppercase

# Returns a function to generate node names with given prefix
def make_name_generator (prefix = '', length = 1):
  wheels = [{ 'position': None, 'max': len(ascii_uppercase), 'values': list(ascii_uppercase)} for _ in range(length)]

  def name_generator ():
    for wheel in wheels:
      if wheel['position'] is None:
        wheel['position'] = 0
      else:
        wheel['position'] += 1
        if wheel['position'] < wheel['max']:
          break
        else:
          wheel['position'] = 0

    return prefix + ''.join(reversed([wheel['values'][wheel['position']] for wheel in wheels]))
  
  return name_generator


# Wrap text on labels
def wrapped (text, width=45, join='\n'):
  return join.join(textwrap.wrap(text, width=width))
