from flask import Flask, render_template
from random import choice
from descriptions import descriptions
from local_settings import BASEURL

app = Flask(__name__)

@app.route('{}/'.format(BASEURL))
def index():

  pick = choice(descriptions)

  context = {
    'BASEURL': BASEURL,
    'image_id': pick[0],
    'file': pick[1],
    'description': pick[2],
    'color': pick[3],
    'color_string': 'rgb({})'.format(', '.join(map(str, pick[3]))),
    'audio': pick[4]
  }
  return render_template('index.html', **context)